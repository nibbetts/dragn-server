""" A DRAGN Lab Server module for Anaphora Resolution jobs """

from allennlp.predictors.predictor import Predictor
import torch
from time import time
from os import listdir
from os.path import isfile, join
import allennlp_models.coref


IGNORE_REFS = {
    "a", "an", "the", "that", "this", "another",
    "her",  "him", "she", "he", "it",  "we",  "us",   "you",           "they",  "them",
    "hers", "his",              "its", "our", "ours", "your", "yours", "their", "theirs",
    "where", "there", "when", "then", "who", "what", "whose",
    "i", "me", "my", "myself", "mine",
}
NON_THIRD_PERSON = {
    "i", "me", "my", "myself", "mine",
    "we", "us", "our", "ours",
    "you", "your", "yours",
}
JOINER = " ~ "



# NOTE/ TODO: we're losing some data with belonging and and's with current replacement scheme.

def resolve(text, sub=False, skip_sub=[]):
    """ Perform anaphora resolution on a text, swapping out references.
        Returns the resolved text.
        
        Use True in second argument to use the first span for each ref.
        
        The third argument is an optional list of phrases not to be substituted
        at all. This defaults to an empty list, but using None will use a
        default ignore list NON_THIRD_PERSON, which includes specifically
        first- and second-person terms to be ignored, resulting in only a
        substitution of third-person and other such terms.
        NOTE: Terms in this list are not case-sensitive."""
    # TODO: Fashion it to return a dictionary of refs where:
    #   {ref : [list of strings ref replaced]}

    # Initialization
    assert (torch.cuda.current_device() == 0), "No GPU available on server!"
    print("Loading models...")
    coref_model_url = "https://storage.googleapis.com/allennlp-public-models/coref-spanbert-large-2020.02.27.tar.gz"
    coref_predictor = Predictor.from_path(coref_model_url, cuda_device=torch.cuda.current_device())
    used_refs = set()
    if skip_sub is None: skip_sub = NON_THIRD_PERSON
    else: skip_sub = {s.lower() for s in skip_sub}

    # Get coreferences
    result = _get_coref_prediction(coref_predictor, text)
    clusters, document, result = result['clusters'], result['document'], result['document'].copy()
    refs = [_get_first_span(c, result) for c in clusters] if sub \
        else [_get_name(c, result, used_refs) for c in clusters]
    # print(f"Coref Clusters: {clusters}")

    # Substitute in the coreferences
    for j, c in enumerate(clusters):
        for span in c:
            phrase = " ".join([document[m] for m in range(span[0], span[1]+1)])
            if phrase.lower() not in skip_sub:
                for m in range(span[0], span[1]+1):
                    result[m] = None
                result[span[0]] = refs[j]
    result = filter(lambda s: s is not None, result)
    return ' '.join(result)


def resolve_multiple(text_list, sub=False, skip_sub=[]):
    """ Same as 'resolve' function, only takes multiple pieces of text and
        treats them as one. Use a list of strings in place of a string for the
        first argument. Other arguments are the same."""

    text = JOINER.join(text_list)
    result = resolve(text, sub=sub, skip_sub=skip_sub).split(JOINER)
    assert (len(text_list) == len(result)), "Internal JOINER token not functioning properly!"
    return result


#------------------------------------------------------------------------------#
# Wrappers to simplify server calls:

def resolve_sub(text, skip_sub=NON_THIRD_PERSON):
    """ Same as resolve, but
        substitute in the first span in place of all ref tokens."""
    return resolve(text, sub=True, skip_sub=skip_sub)

def resolve_multiple_sub(text_list, skip_sub=NON_THIRD_PERSON):
    """ Same as resolve_multiple, but
        substitute in the first span in place of all ref tokens."""
    return resolve_multiple(text_list, sub=True, skip_sub=skip_sub)


#------------------------------------------------------------------------------#

# Run coref predictor on text
def _get_coref_prediction(predictor, text):
    start_time = time()
    result = (predictor.predict(
        document=text
    ))
    delta = time() - start_time
    print("Coref Prediction Time: {:1.2f}".format(delta))
    return result

# Find something unique to call each reference
def _get_name(cluster, document, used_refs):
    tokens = []
    for span in cluster:
        for i in range(span[0], span[1]+1):
            tokens.append(document[i])
    name = None
    for t in tokens:
        if t and t.lower() not in IGNORE_REFS:
            name = t
            break
    if name is None:
        name = tokens[0]
    k = 0
    while name + str(k) in used_refs: k += 1
    used_refs.add(name + str(k))
    return name + str(k)

# Use the first span of a cluster to call each reference instead of new token
def _get_first_span(cluster, document):
    # NOTE: this works even if the first span is one of the ignore terms in
    #   skip_sub, so that other spans will be replaced to match this one.
    # TODO: This (above) may not be the desired behavior!
    span = cluster[0]
    tokens = []
    for i in range(span[0], span[1]+1):
        tokens.append(document[i])
    name = " ".join(tokens)
    return name
