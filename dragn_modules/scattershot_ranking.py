import chitchat_dataset as ccc
import spacy
from os.path import exists
import numpy as np
from scipy import spatial

nlp = spacy.load("en_core_web_sm")
SCAFFOLD_CORPUS = "chitchat"
EMBEDDING_LEN = 96 #spacy doc vectors are 96 elements long

def embed(text):
    doc = nlp(text)
    return doc.vector

def list_embed(texts):
    embeddings = []
    for text in texts:
        embeddings.append(embed(text))
    return embeddings

def find_scaffold_match(v_context, num_matches=1, dist_cutoff = 0.3):
    # find scaffold entry that most closely matches v_context[-1]

    print(cached_scaffold.shape)
    distances = spatial.distance.cdist([v_context[-1]], cached_scaffold, metric='cosine')
    inds = np.argsort(distances)
    return cached_scaffold[inds[0][0]] # For now, just take the top match

def rank(context, sentences):
    chat_history = context['history'][-2:] # for now, only use a context of len 2

    v_context = list_embed(chat_history)
    v_sentences = list_embed(sentences)
    v_scaffold = find_scaffold_match(v_context)

    # calculate distances from each sentence to v_scaffold
    distances = spatial.distance.cdist([v_scaffold],np.vstack(v_sentences),metric='cosine')
    scores = 2-distances[0]
    return scores

def load_scaffold(corpus, LIMIT=10000):
    scaffold = []
    print('loading corpus for scattershot ranker...')
    print('scattershot number-of-conversations limit is', LIMIT)
    FILENAME = 'scattershot_data/scattershot_chitchat_corpus' + str(LIMIT) + '.bin'
    if corpus == "chitchat":
        if exists(FILENAME):
            scaffold = np.fromfile(FILENAME,dtype='int16').reshape(-1, EMBEDDING_LEN)
        else:
            dataset = ccc.Dataset()
            for convo_id, convo in list(dataset.items())[:LIMIT]:
                messages = []
                for message in convo["messages"]:
                    for line in message:
                        messages.append(line["text"])
                #print(messages)
                #input('>')
                scaffold += list_embed(messages)
                #print("done embedding")

            # Save the embedded corpus to a file cache in order
            # to speed up subsequent loads
            scaffold = [msg for msg in scaffold if len(msg) == EMBEDDING_LEN]
            scaffold = np.vstack(scaffold).astype('int16')
            scaffold.tofile(FILENAME)
    else:
        raise ValueError("Unknown scaffold corpus for Scattershot Ranker: " + corpus)
    print(len(scaffold), 'sentences embedded.')
    if len(scaffold) < 1:
        raise ValueError("Unknown error while loading scaffold corpus. No conversations retrieved.")
    return scaffold

cached_scaffold = load_scaffold(SCAFFOLD_CORPUS)

#debug stuff
context = {}
context['history'] = ['Hello there!','Hi, glad to meet you. Do you like books?']
sentences = ['Yes','No','Yes, I do like books','No, I don\'t like books','Hello, there!','What is the square root of an isoceles triangle?','I don\'t like this conversation']
print('Context: ', context, 'sentences', sentences)
rankings = rank(context, sentences)
print(rankings)
