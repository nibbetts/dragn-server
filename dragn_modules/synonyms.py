""" A DRAGN Lab Server module for jobs making use of existing word/sentence embeddings,
    specifically relating to synonym testing.

    NOTE: This module makes use of the embeddings module, accessed through SERVER,
    a variable assigned by the server at module load time.
"""

import numpy as np
from scipy.spatial.distance import cdist
if "SERVER" not in dir(): SERVER = None # Should be defined by the server.


_LETTERS = set(' abcdefghijklmnopqrstuvwxyz') # And space



def synonym_distance(sentence, synonyms, space='glove', _syn_vecs=None,
        metric='euclidean', *args, **kwargs):
    """ Finds how related each in a list of words is to any words in a given sentence.
        May be used for topical classification of the sentence, if the given
        synonym list is a list of topics.

    ARGUMENTS:
        sentence (str): The sentence in which we are seeking for synonyms.
            This will be split, and each word compared separately,
            not embedded as a whole.
            Sentence will be cleaned to remove punctuation and capitals.

        synonyms (List of str): List of words we are looking for synonyms
            (or identities) of in the sentence.
            These will NOT be cleaned, so must not contain punctuation.
            Treatment of capitals may depend on the embedding space.

        space (Optional, str): Name of the embedding space to use for word
            distance comparisons. Must be one of those found by list_spaces(),
            which are those in embeddings/txt or embeddings/pkl.
            File extensions will be ignored.
            If an embedding file has not yet been parsed, it will be.
            If an embedding file has been updated, it will be reparsed.

        _syn_vecs: Internal use only, to speed up the wrappers which do multiple
            calls at once by only vectorizing synonyms once.

        metric (string): allows any metric scipy.spatial.distance.cdist does.
            Defaults to 'euclidean'.

        *args, **kwargs: takes extra arguments for metric, if any.

    RETURNS:
        matches (List of str): Same order as synonyms, but each is the word from
            the sentence which is the closest match to the synonym.
        distances (List of float): Same order as synonyms, but each is the
            distance from that synonym to the corresponding word from matches.
        NOTE: For human readability, you can use:
            list(zip(synonyms, distances, matches))
        
    NOTE: As sentence words not found in the space are ignored, if no words
        in a sentence are found distances will contain infinite values. Likewise,
        if a given synonym is not found, it's distance will also be infinite,
        and the sentence words chosen will be arbitrary.
        
    NOTE: If the sentence contains multiple synonyms to a given word in,
        synonyms only the closest in the embedding space is reported for each.
        This remains true for the synonym_threshold function."""

    words2vecs = space[1:] if space[0] is None else SERVER.get_space(space)
    n = len(words2vecs)

    # Find word vectors
    sent_words = _process_sentence(sentence)
    sent_vecs = [(words2vecs[w] if w in words2vecs else _inf_vec(n)) \
        for w in sent_words]
    syn_vecs = [(words2vecs[w] if w in words2vecs else -_inf_vec(n)) \
        for w in synonyms] if _syn_vecs is None else _syn_vecs

    # Find distances; result is indexable by (sent_word_i, synonym_i)
    distances = cdist(sent_vecs, syn_vecs, metric=metric, *args, **kwargs)

    # Find words that best match, and their distances
    matches = [sent_words[i] for i in np.argmin(distances, axis=0)]
    distances = np.amin(distances, axis=0)
    return matches, distances


def synonym_threshold(sentence, synonyms, threshold=0., space='glove',
        metric='euclidean', *args, **kwargs):
    """ Wrapper for synonym_distance, but only returns whether a 'synonym'
        for each word was present in the sentence, where synonymity is defined
        by a distance threshold.

    ARGUMENTS:
        sentence: See s>ynonym_distance.

        synonyms: See synonym_distance.

        threshold (Optional, float): Maximum distance in the embedding space
            words may be in order to be defined as synonyms.
            Defaults to 0, meaning the words must be the same, not just synonyms.

        space: See synonym_distance.

        metric: See synonym_distance.

    RETURNS: An array of boolean values matching the structure and order of
        synonyms, where for each value True indicates that a synonym to the
        word in synonyms was found in the sentence, and False indicates that
        none was found. False will be returned for synonyms not in the space."""

    _, distances = synonym_distance(
        sentence, synonyms, space, metric, *args, **kwargs)
    return distances <= threshold


def synonym_distance_multiple(sentences, synonyms, space='glove',
        metric='euclidean', *args, **kwargs):
    """ Same as synonym_distance, only packages multiple calls together, where
        each has the same synonyms, but a different sentence.
        This is an optimization to save time with loading and offloading the
        embedding space to RAM or GPU, and to save time vectorizing things.

    ARGUMENTS: The only argument difference is that the first argument is a list of
        strings instead of a string.
        
    RETURNS: List of returns from calls to synonym_distance, in the same
        order as sentences.
        
        This means this is a list of tuples (matches, distances), one for each
        sentence, where matches and distances correspond to the entries in
        synonyms."""

    words2vecs = SERVER.get_space(space)
    syn_vecs = np.array([
        (words2vecs[w] if w in words2vecs else -_inf_vec(len(words2vecs))) \
        for w in synonyms])
    return [
        synonym_distance(s, synonyms, [None, len(words2vecs), words2vecs],
            syn_vecs, metric, *args, **kwargs)
        for s in sentences]


def synonym_threshold_multiple(sentences, synonyms, threshold=None,
        space='glove', metric='euclidean', *args, **kwargs):
    """ Same as synonym_threshold, only packages multiple calls together, where
        each has the same synonyms, but a different sentence.
        This is an optimization to save time with loading and offloading the
        embedding space to RAM or GPU, and to save time vectorizing things.

    ARGUMENTS: The only argument difference is that the first argument is a list of
        strings instead of a string.

    RETURNS: List of returns from calls to synonym_threshold, in the same
        order as sentences.
       
        This means this is a list of lists of boolean values,
        where each list is a representation of which words in synonyms were
        found to have synonyms in the corresponding sentence."""
    # NOTE: For coding optimization and user comprehension, I have described
    #   this function as a wrapper around synonym_threshold but coded it as a
    #   wrapper around synonym_distance_multiple.

    sentence_results = synonym_distance_multiple(
        sentences, synonyms, space, metric, *args, **kwargs)
    return [d <= threshold for _, d in sentence_results]



#------------------------------------------------------------------------------#
# Internals


# Helper function to get vector of inf's
def _inf_vec(n):
    v = np.ones(n)
    v[:] = np.inf
    return v

# Clean by lowering and filtering sentence to only letters, then split
def _process_sentence(sentence):
    clean_sent = ""
    for c in filter(lambda c: c in _LETTERS, sentence.lower()):
        clean_sent += c
    return list(filter(lambda w: w != "", clean_sent.split(" ")))