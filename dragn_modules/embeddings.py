""" A DRAGN Lab Server module for loading, storing, and manipulating
    word/sentence embeddings.

    NOTE: This module attempts to keep a few embedding spaces in memory at a time,
    using an LRU cacheing system, preventing the necessity of reloading every time
    if you are making use of a small number of spaces at a time.
"""

import numpy as np
import pickle
from tqdm import tqdm
from os.path import exists, isfile, getmtime, getctime, join
from os import listdir
from collections import OrderedDict
from time import time


# Module-wide constants
TXT_PATH = "data/embeddings/txt"
PKL_PATH = "data/embeddings/pkl"
BASIC_SPACES = {
    'glove'    : "glove.840B.300d",
    'fasttext' : "crawl-300d-2M-subword",
    'word2vec' : "GoogleNews-vectors-negative300",
}
EMBEDDING_CACHE_SIZE = 3

# TODO: sentence embeddings


#------------------------------------------------------------------------------#
# For client usage as well as for other server modules

def list_spaces():
    """ Get an up-to-date list of available embedding spaces,
        regardless of whether or not the system has already parsed them.
        The shortcut names given in BASIC_SPACES will be included
        as duplicates to their referents."""
    print(f"Note that the first {len(BASIC_SPACES)} spaces listed are simply shorthand for later ones.")
    return list(BASIC_SPACES.keys()) + list({
        name[:-4]
        for name in listdir(TXT_PATH) + listdir(PKL_PATH)
        if (isfile(join(TXT_PATH, name)) or isfile(join(PKL_PATH, name))) \
            and name[0] != '.'
    })


def in_space(words, space='glove'):
    """ Function to check which words are found in the embedding space.

    ARGUMENTS:
        words (List of str): Words to check for.

        space (Optional, str): Name of the embedding space to check in.
            Must be one of those in EMBEDDING_SPACES.

    RETURNS: An array of boolean values with the same order as words.
        Each is True if the word is found in the space."""

    words2vecs = get_space(space)
    return [w in words2vecs for w in words]


def reset_cache(capacity=EMBEDDING_CACHE_SIZE):
    """ A method to clear the embedding space cache, and/or change its size.
        Estimate ~8 gigabytes of constant ram usage per embedding space."""
    # TODO: implement a method to adjust the cache size without resetting it?
    _space_cache = _LRUCache(capacity)


def get_vectors(words, space='glove'):
    """ Get the vectors for a list of words, in the same order.
        For all vectors instead, try using dict.values."""
    words2vecs = get_space(space)
    return [words2vecs[w] for w in words]


#------------------------------------------------------------------------------#
# Mainly for server module usage, though clients may

def get_vocab(space='glove'):
    """ Get the entire list of words included in an embedding space.
        WARNING: This may be too large to send and receive properly, or may
        take a long time. Even within server modules, if you can use the
        dict.keys object directly that will likely be faster."""
    words2vecs = get_space(space)
    return list(words2vecs.keys())


# Parses to get the space if there is a .txt newer than a matching .pkl,
#   and checks cache for an up-to-date space before reloading it.
def get_space(name):
    """ Loads an embedding space and returns dict(words->vecs).
        WARNING: Not recommended for client usage, as the returns sent will
        likely comprise several gigabytes. This function exists mainly to be
        used by other server modules."""
    if name[-4:] in [".pkl", ".txt"]: name = name[:-4]
    if name in BASIC_SPACES: name = BASIC_SPACES[name]
    txt_path = TXT_PATH + f"/{name}.txt"
    pkl_path = PKL_PATH + f"/{name}.pkl"

    # Check if we need to update the space's .pkl
    if exists(txt_path):
        if not exists(pkl_path) or \
                max(getmtime(pkl_path), getctime(pkl_path)) < \
                max(getmtime(txt_path), getctime(txt_path)):
            _space_timestamps[name] = time()
            space = _parse(txt_path, pkl_path)
            _space_cache[name] = space
    elif not exists(pkl_path):
        raise FileNotFoundError(
            f"No .pkl or .txt found in embeddings folders for {name}") 
    # If we get here, the .pkl file exists now and is up to date
    if name in _space_cache and _space_timestamps[name] >= \
            max(getmtime(pkl_path), getctime(txt_path)):
        space = _space_cache[name]
    # Either hasn't been loaded, or the .pkl was updated separately from a .txt
    else:
        _space_timestamps[name] = time()
        space = pickle.load(open(pkl_path, 'rb'))
        _space_cache[name] = space

    return space


#------------------------------------------------------------------------------#
# Internals


# Parses embeddings at path1 to a pickle at path2
def _parse(path1, path2):    
    with open(path1, 'rt') as f:
        space = {}
        for line in tqdm(f.readlines()):
            parts = line.split(' ')
            if len(parts) < 10: continue
            space[parts[0]] = np.array([float(p) for p in parts[1:]])
    with open(path2, 'wb') as f:
        pickle.dump(space, f)
    return space

# To cache a limited number of large objects in
class _LRUCache:
    def __init__(self, capacity):
        self.capacity = capacity
        self.cache = OrderedDict()
        self.keys = self.cache.keys # The keys() function

    def __getitem__(self, key):
        # Throws exception if not found
        value = self.cache.pop(key)
        self.cache[key] = value
        return value

    def __contains__(self, key):
        return key in self.cache

    def __setitem__(self, key, value):
        try:
            self.cache.pop(key)
        except KeyError:
            if len(self.cache) >= self.capacity:
                self.cache.popitem(last=False)
        self.cache[key] = value


#------------------------------------------------------------------------------#
# Module-wide variables and code to execute upon loading

# Keep track of loaded embedding spaces and times to avoid reloading if they
#   haven't been updated or dropped out of memory for lack of space.
_space_cache = _LRUCache(EMBEDDING_CACHE_SIZE)
_space_timestamps = {}
