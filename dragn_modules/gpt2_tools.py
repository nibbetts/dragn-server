import gpt_2_simple as gpt2


DATA_PATH = "data/gpt2_simple"

def gpt2_loader():
    sess = gpt2.start_tf_sess()
    gpt2.load_gpt2(sess, checkpoint_dir=DATA_PATH)
    return sess

def generate_QA(question):
    sess = SERVER.get_model("gpt2", gpt2_loader)

    input_sentence = question.strip()
    prefix = '''Q: What do you like to do? A: I like to go fishing.
                Q: Where do you live? A: I live in England.
                Q: Who are your family? A: My mother's name is Lucy and my father's name is John.
                Q: What is your occupation? A: I am a writer.
                Q: When is your birthday? A: My birthday is May 5th.
                Q: What do you like to write? A: I like to write letters to friends and family.
                Q: When and where were you born? A: I was born in Wales in 1743.
                Q: Do you follow a religion? A: Yes, I am Catholic.
                Q: What types of books do you like to read? A: I like to read romance books.
                Q: Who is your best friend? A: Lacy is my best friend; we do everything together.
                Q: What is your favorite food? A: I love steak and mashed potatoes.
                Q: When did you start writing? A: I started writing in school.
                Q: How is your sister doing? A: She is doing well; she just got married.
                Q: '''
    prefix += input_sentence + " A: "

    responses = gpt2.generate(sess, run_name='run1', length=50, checkpoint_dir=DATA_PATH, include_prefix=False, return_as_list=True, prefix=prefix)[0]
    responses = responses.replace(prefix, "", 1)
    responses = responses[:responses.index(".")]
 
    return responses

if __name__ == "__main__":
    sess = gpt2_loader()
    generate_QA("How far away is the moon?")
