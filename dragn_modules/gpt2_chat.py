import sys
from dragn_modules.elizabeth_gpt2_data.conversation_engine import conversation_engine

ce = conversation_engine()
response = ce.start()

def get_gpt2_response(text):
    return ce.chat(text, verbose=True)

if __name__ == "__main__":
  while(1):
    text = input(response+'\n')
    if text:
        response = ce.chat(text, verbose=True)
