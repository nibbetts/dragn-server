""" A DRAGN Lab Server module for loading, storing, and manipulating
    various models and datasets, which we will call models too in this file.

    NOTE: This module attempts to keep a few things in memory at a time,
    using an LRU cacheing system, preventing the necessity of reloading every time
    if you are making use of a small number of things at a time.
"""

# Module-wide constants
from dragn_modules.embeddings import _LRUCache
MODEL_CACHE_SIZE = 5


#------------------------------------------------------------------------------#
# Mainly for server module usage

def get_model(model_name, loader_function, force_reload=False, *args, **kwargs):
    """ For server modules to store and retrieve loadable models, or even loadable datasets,
        through the server memory LRU cache. Not useful for client usage, as live objects cannot be
        passed from server to client, and large datasets should not (see 'model_*' functions instead).
        Useful for large things that we want to keep in memory if possible, but want to let go
        of if there is not enough memory for everything. Accessible between server modules as well.

        Will load a new model if the loader_function is different than another one cached.
        To force a reload with the same function (say you broke the model) use force_reload=True.

        PARAMETERS:
        model_name (str): name of object to store in or get from cache.
        loader_function (callable): function which loads the model in case it's not found in cache.
        force_reload (bool): Defaults to False.
            If true, forces model to reload despite potential presence in cache.
        *args, **kwargs: arguments to loader_function.

        RETURNS: the loaded or cached model object.
    """
    if (model_name, loader_function) in _model_cache and not force_reload:
        return _model_cache[(model_name, loader_function)]
    else:
        m = loader_function(*args, **kwargs)
        _model_cache[(model_name, loader_function)] = m
        return m


#------------------------------------------------------------------------------#
# For client usage, as well as other server modules,
# though those typically have more direct access

def list_models():
    """ Get a list of loaded models, and the function names and addresses that loaded each."""
    models, functions = zip(*list(_model_cache.keys()))
    f_names = [f.__name__ for f in functions]
    return zip(models, f_names, functions)

def reset_cache(capacity=MODEL_CACHE_SIZE):
    """ A method to clear the model cache, and/or change its size.
        We're guessing ~4 gigabytes of constant ram usage per model."""
    _model_cache = _LRUCache(capacity)

def model_get(model_name, attr_name, loader_function=None):
    """ Get an attribute from a model.
        Normally models are used by other modules, but this allows client probing.

        The loader_function parameter is made optional to allow client usage,
        but if there is a name collision the model getter will fail.
        Also finding the model is slower if loader_function is None.
        Thus, other server modules should always specify this parameter."""
    model = _find_model_by_name(model_name, loader_function)
    return getattr(model, attr_name)

def model_set(model_name, attr_name, new_value, loader_function=None):
    """ Set an attribute within a model.
        Normally models are used by other modules, but this allows client manipulation.
        See 'model_get' for explanation of loader_function."""
    model = _find_model_by_name(model_name, loader_function)
    setattr(model, attr_name, new_value)

def model_call(model_name, function_name, loader_function=None, *args, **kwargs):
    """ Call a function on a model.
        Normally models are used by other modules, but this allows client usage.
        See 'model_get' for explanation of loader_function."""
    model = _find_model_by_name(model_name, loader_function)
    return getattr(model, function_name)(*args, **kwargs)


#------------------------------------------------------------------------------#
# Internals

# Searches through the models to find matches. Specific if loader_function is not None.
# If multiple matches, raises a ValueError.
def _find_model_by_name(model_name, loader_function):
    if model_name[0] == '_': raise ValueError(f"Client access to {model_name} is not authorized")
    if loader_function is not None:
        return _model_cache[(model_name, loader_function)]
    models, functions = zip(*list(_model_cache.keys()))
    found = None
    for i, m in enumerate(models):
        if m == model_name:
            if found: raise ValueError(f"Unable to locate {model_name} because multiple models with that name exist")
            else: found = functions[i]
    return _model_cache[(model_name, found)]

# Keep track of loaded models to avoid reloading if they
#   haven't dropped out of memory for lack of space.
_model_cache = _LRUCache(MODEL_CACHE_SIZE)
