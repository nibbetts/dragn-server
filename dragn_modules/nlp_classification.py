""" A DRAGN Lab Server module for jobs making use of existing word/sentence embeddings,
    specifically relating to word and sentence classification.

    NOTE: This module makes use of the embeddings module, accessed through SERVER,
    a variable assigned by the server at module load time.
"""

import numpy as np
from itertools import chain
from scipy.spatial.distance import cdist
from collections import Counter

if "SERVER" not in dir(): SERVER = None # Should be defined by the server.
# from dragn_modules.synonyms import _process_sentence

DEFAULT_STOP_WORDS = ["a", "an", "the"]



def bow_sentence_classification_knn(sentences, clusters, K=1,
        stop_words=DEFAULT_STOP_WORDS, space="glove", metric='euclidean',
        *args, **kwargs):
    """ Use K Nearest Neighbors algorithm to categorize a sentence
        based on exemplars from N clusters,
        where each sentence is converted into a Bag of Words embedding
        (vector average of words in sentence). Exclude stop words from
        averages and clusters, as well as words not found in the space.

        The KNN algorithm does not look for the nearest cluster centroid,
            but rather finds the K nearest exemplars, and these act as votes
            to categorize a sentence as belonging to their cluster.
            The most popular vote wins, or a stalemate (tie in votes) occurs.

        Parameters:
            sentences (list of strings): List of sentences to categorize,
                where each string is a sentence.
            clusters (list of lists of strings): List of clusters,
                where each cluster is a list of word or sentence strings.
                Sentences will be broken into words and BOW'ed (averaged),
                so if you don't want that enter all as separate words.
            stop_words (list of strings): words to ignore when turning sentences
                into bags of words - whether from the inputs or the clusters.
                Defaults to the values in DEFAULT_STOP_WORDS.
            space (string): embedding space to use.
            metric (string or callable): distance metric to use.
                Defaults to 'euclidean'. Accepts any useable by
                scipy.spatial.distance.cdist.
            *args, **kwargs: accepts arguments for metric, if needed.

        Returns:
            A list (length and position corresponding to input sentences) of
            cluster indeces. Where there was a stalemate, the index will
            be None.

        Raises: ValueError if a sentence or cluster entry has no acceptable
            recognized words.
    """
    # TODO: Idea, go until any cluster gets K votes if don't want ambiguous
    #   non-answers? Sometimes we want to know though that it was ambiguous, though....

    # Get vectors and indeces
    sent_vecs = [bag_of_words(s, space, stop_words) for s in sentences]
    exemplars = list(chain.from_iterable(
        [[bag_of_words(s, space, stop_words) for s in c] for c in clusters]))
    indeces = list(chain.from_iterable( # Cluster index for each exemplar
        [len(c) * [i] for i, c in enumerate(clusters)]))

    # Compute distance matrix
    distances = cdist(sent_vecs, exemplars, metric=metric, *args, **kwargs)

    # Find KNNs of each sentence
    neighbors = []
    rows = np.arange(len(distances))
    for i in range(K):
        mins = np.argmin(distances, axis=1)
        neighbors.append(mins)
        if i != K-1: distances[rows, mins] = np.inf
    neighbors = zip(*neighbors) # Transpose

    # Find neighbors' classifications (cluster indeces)
    neighbors = [[indeces[n] for n in votes] for votes in neighbors]    

    # Classify sentences based on neighbors
    counts = [Counter(votes) for votes in neighbors]
    top_2 = [c.most_common(2) for c in counts]
    print(top_2)
    return([(votes[0][0] if len(votes) < 2 or votes[0][1] != votes[1][1]
        else None) for votes in top_2]) # Returns top vote if not tied with 2nd

def bag_of_words(sentence, space, stop_words):
    """ Process and convert a sentence into a bag of words (average) embedding,
        skipping stop_words and words not present in the space.
        The space parameter is the name of the space, not the dictionary.
        Raises ValueError if sentence has no recognized acceptable words."""
    words2vecs = SERVER.get_space(space)
    words = SERVER.get_module("synonyms")._process_sentence(sentence)
    vecs = [words2vecs[w] for w in words \
        if w in words2vecs and w not in stop_words]
    if not len(vecs): raise ValueError("No recognized words to bag")
    return np.mean(vecs, axis=0)


#------------------------------------------------------------------------------#
# Internals
