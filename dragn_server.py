#!/usr/bin/env python3
""" 
BYU DRAGN Lab Server
By: Nathan Tibbetts
July 2020
"""

import sys
import json
import socket
from os import listdir, execv, remove, rename, getpid, path
from os.path import isfile, join, getctime, getmtime, getsize, exists
import inspect
from time import time
from datetime import datetime
from importlib import import_module, invalidate_caches, reload
from random import randint
from inspect import isfunction, getmembers, getmodule, isbuiltin, ismethod, isclass, ismodule
from dragn_client import DEFAULT_IP, DEFAULT_PORT, DEFAULT_URL, _send_msg, _recv_msg
from traceback import format_exception
import subprocess
import multiprocessing
from collections import OrderedDict
from psutil import virtual_memory, Process
import gc

# Add dragn_modules to the path
currentdir = path.dirname(path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, currentdir + '/dragn_modules/')

# Globals
PORT_MIN, PORT_MAX = 1024, 65535
MODULES_FOLDER = "dragn_modules"
LOGS_FOLDER = "dragn_logs"
LOG_EXTENSION = ".log"
CURRENT_LOG = join(LOGS_FOLDER, '0' + LOG_EXTENSION)
MEMORY_RATIO = .5
LOG_SIZE = 2000000
LOG_COUNT = 5
# KEYWORDS   = ["PRINT", "RETURN", "EXCEPTION", "SHUTDOWN", "RESTART", "LIST", "PING", "DOCS", "CONSTANTS"]



# The system time for each module imported, to compare with dates on their
#   files to know if we need to reload them
_module_timestamps = {}

# A class used for modules to share information with each other. The could just
#   reload each other, unless they want large data that is already in memory.
class Shared:

    """ TODO: In order to parallelize the server and server multiple clients at once,
        we need to:
            - Make each model a separate process, serving answers
            - Make either a manager for the / each space dict, or make them each a separate process as well.
            - possibly in the future have duplicates of common ones, and grab any available copy of a model or space. Semaphore?
            - avoid copying any models. Perhaps to unsafely share memory we need threads - would be CPU bound anyway, so why not?

        NOTES:
        - Keeping modules in memory requires larger stack copy unless we do a full new process - but how would function call work? Wouldn't be able
            to easily access shared things like models. Also runs into problems with updating modules - server will always be behind the workers,
            and they'll have to reload things anyway, deleting whatever shared data they had within.
        - could load all modules used by a module before running function - but that's hard and hacky.
        - if instead we don't keep modules in memory, we either have to never load them, or we have to delete them. Deleting them won't work
            on functions imported from modules, and won't work on those loaded into other modules, so the lab members would have to always
            call a wrapped import anyway, defeating the purpose of avoiding them being forced to do so.
        * so, best solution: don't import any modules / delete after each, so that copied stack has to reload it. Can avoid importing
            by splitting off processes before we load the module concerned? In this case, server manages only references to models and spaces workers,
            immediately splitting off process when new client connects. But then original server can't load models or spaces itself,
            but also has references to the workers that do so, and those workers maintain references to the models etc.
            models.py and embeddings.py must be part of the server that doesn't need constantly reloaded, separate from other modules.
            This means other modules have to ask the server for these two, rather than just importing them? They could be a class with all those
            methods already defined on them...?
        ! Have to have LRU in separate memory as well, because it is changing. Only its pointer does not change.
        ! Have to either lock model manager from being offloaded while client fucntion is being run, or have to add abstraction layer
            removing all direct access, OR BEST: build a wrapper aroudn the proxy that checks if still in memory every time its used and temp locks it.
    """

    def get_module(self, name):
        """ Gets a server module, checking for code changes and updating if needed.
            Simply importing a module will only use the one already in memory."""
        return _wrapped_import(name) # TODO: change for multiprocessing. Won't work the same way...

    def get_space(self, space="glove"):
        """ Gets or loads an embedding space from the embeddings folder or server cache."""
        embeddings = _wrapped_import("embeddings")
        return embeddings.get_space(space)

    def get_model(self, model, loader_function, force_reload=False, *args, **kwargs):
        """ Gets or loads a loadable model, dataset, etc through server cache."""
        models = _wrapped_import("models")
        return models.get_model(model, loader_function, force_reload=force_reload, *args, **kwargs)

SERVER = Shared() # A limited handle for the modules to access

'''
LIBRARIAN = multiprocessing.Manager()
LIBRARIAN.library = ModelCache(MEMORY_RATIO)

def __init__(self):
    # TODO: setup Library and proxy

def get_model(name, loader_function, force_reload=False, *args, **kwargs):
    # Somehow need to lock the library?
    # Check for model presence
    # Register & Proxy if needed, return proxy
    # Unlock the library
    pass

class ModelCache:
    """ The 'Library' that is shared between processes."""
    def __init__(self, memory_ratio):
        self.memory_cap = 0
        self.memory_cap = self.set_memory_cap(memory_ratio)
        self.cache = OrderedDict()
        self.process = Process(getpid())

    def __getitem__(self, model_name, loader_name):
        # Throws exception if not found
        key = (model_name, loader_name)
        value = self.cache.pop(key)
        self.cache[key] = value
        return value

    def __setitem__(self, key, model):
        with _PrintLogger():
            if key in self.cache:
                print(f"LIBRARIAN >>> Loaded model {key} twice; discarding second")
                # NOTE: This should be a rare occurrence;
                #   If it is happening frequently there may be a bug,
                #   or perhaps there is too little memory for all the commonly-used models.
            else:
                self.cache[key] = model
                print(f"LIBRARIAN >>> Adding model {key} to cache")
                self._clean_up_cache()

    def set_memory_cap(self, memory_ratio):
        old_cap = self.memory_cap
        self.memory_cap = int(virtual_memory().total * memory_ratio)
        if self.memory_cap < old_cap:
            with _PrintLogger():
                self._clean_up_cache()

    def _clean_up_cache(self):
        """ Clean up overfull cache. Don't remove last item."""
        while len(self.cache) > 1 and self.process.memory_info()[0] > self.memory_cap:
            m = self.cache.popitem(last=False)
            print(f"LIBRARIAN >>> Removing model {m} from cache")
            del(m)
            gc.collect()
'''

def server(address=None, port=DEFAULT_PORT):
    # log_size: in bytes. Not a maximum, but rather a threshold beyond which logs are cycled.
    # log_count: log files >= this number are deleted when logs are cycled.

    with _PrintLogger():
        # NOTE: Port number may increment once
        #   to get an open port, and the Client will do likewise.
        host_name = socket.gethostname()
        if host_name == "Balrog": host_name = "maia" # TODO: shouldn't...
        host_ip   = socket.gethostbyname(host_name)
        if port is None:
            port = randint(PORT_MIN, PORT_MAX)
        elif port < PORT_MIN:
            raise ValueError("Must use port numbers between 1024 and 65535")
        if address is None:
            address = DEFAULT_URL.format(host_name)
        server_address = (address, port)

        # Specify the socket type, which determines how clients will connect.
        server_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # Enable reuse of socket in case needed by forked child processes
        server_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try: server_sock.bind(server_address) # Assign this socket to an address.
        except:
            server_address = (address, port+1) # Client & Server flexible btw 2 ports in case restart
            server_sock.bind(server_address)
        server_sock.listen(1)                # Start listening for clients.

        print( "DRAGN Lab Server started at:")
        print(f"   Host Name: {host_name}")
        print(f"   IP:        {host_ip}")
        print(f"   Port:      {port}")
        print(f"   Timestamp: {timestamp()}")

        run = True
        while run: # TODO: eventually make async so can receive multiple jobs at once and listen for local commands as well
            # Wait for a client to connect to the server.
            print("SERVER >>> Waiting for a connection...\n")
            connection, client_address = server_sock.accept()
            child_spawned = False

            try:
                # Receive data from the client.
                in_data = _recv_msg(connection)
                client_name = socket.gethostbyaddr(client_address[0])[0]
                print(f"SERVER >>> Connection accepted from: {client_name}, {client_address} at {timestamp()}")
                print(f"SERVER >>> <Received '{in_data}' from client>")

                # Interpret the command
                module_name, function_name, args, kwargs = in_data
                out = None

                # Server Restarter
                if module_name == "RESTART":
                    print("SERVER >>> Recieved restart command from client, pulling changes and restarting server")
                    _pull_changes()
                    run = False

                # Server Shutter-Downer
                elif module_name == "SHUTDOWN":
                    print("SERVER >>> Recieved shutdown command from client, shutting down server")
                    run = False

                # Functionality Lister
                elif module_name == "LIST":
                    # Here, function_name is the module name to list functions of
                    if function_name is None:
                        out = [f[:-3] for f in listdir(MODULES_FOLDER) \
                            if isfile(join(MODULES_FOLDER, f)) \
                            and f[-3:] == ".py"]
                    else:
                        constants = len(args) and args[0] == "CONSTANTS"
                        module = _wrapped_import(function_name)
                        members = dict(getmembers(module))
                        # We really want just functions (or constants if asked)
                        out = [f for f in dir(module) \
                            if _is_valid_object(members[f], module, f, constants)] # TODO: currently constants also lists functions

                # Documentation Getter
                elif module_name == "DOCS":
                    if "." in function_name: # 'module.function' behavior
                        module_name, function_name = function_name.split(".")
                    elif len(args): # Extra argument for function
                        module_name, function_name = function_name, args[0]
                    else: # Module alone, no function
                        module_name, function_name = function_name, None

                    module = _wrapped_import(module_name)
                    if function_name is None:
                        out = module.__doc__
                    else:
                        function = getattr(module, function_name)
                        if _is_valid_object(function, module, function_name):
                            if isfunction(function):
                                out = function.__doc__
                            else:
                                # In this case it should just be a constant
                                out = function
                        else:
                            raise ValueError(f"Function or constant '{function_name}' not found")

                # Git Puller
                elif module_name == "PULL":
                    with _PrintForwarder(connection):
                        _pull_changes()
                        print("SERVER >>> Git pull complete, modules updated")

                # Connectivity Pinger
                elif module_name == "PING":
                    pass

                # Function Executer
                else:
                    # Find the requested module and function
                    module = _wrapped_import(module_name) # TODO: test if timing makes it not worth moving this into child; otherwise doing so would ensure no module is huge and overloads server with stack-copying
                    function = getattr(module, function_name)
                    # TODO: see if this works, else may have to use queue:
                    # Run the command (module.function) sent by the client
                    child_job(connection, client_name, client_address, function, *args, **kwargs)
                    child_spawned = True

                # Process the received data and send something back to the client.
                if not child_spawned:
                    return_type = ("RETURN" if run else module_name)
                    out_data = [return_type, out] # TODO: don't send if child process will
                    print(f"SERVER >>> Sending {return_type} back to the client") # TODO: remove this???
                    _send_msg(connection, out_data)

            except:
                e = "".join(format_exception(*sys.exc_info()))
                _send_msg(connection, ["EXCEPTION", e])
                print(f"SERVER >>> <{e}>")

            # Make sure the connection is closed securely.
            finally:
                if not child_spawned:
                    connection.close()
                    print(f"SERVER >>> Closing connection from: {client_name}, {client_address}")

            # Server Restarter
            if module_name == "RESTART":
                execv(sys.argv[0], sys.argv)


# Function called as a forked child process
def child_job(connection, client_name, client_address, function, *args, **kwargs):
    # Run the function with the args and kwargs, forwarding prints, catching exceptions,
    #   and respond to the client on the connection, before closing the connection.
    try:
        with _PrintForwarder(connection):
            out = function(*args, **kwargs)
        out_data = ["RETURN", out] # TODO: add timing & printouts
        print(f"WORKER >>> Sending RETURN back to the client") # TODO: fix for multiprocessing-compatible logs. Also remove?
        _send_msg(connection, out_data)
    
    except:
        e = "".join(format_exception(*sys.exc_info()))
        _send_msg(connection, ["EXCEPTION", e])
        print(f"WORKER >>> <{e}>")

    finally:
        connection.close()
        print(f"WORKER >>> Closing connection from: {client_name}, {client_address}") # TODO: fix this too


#------------------------------------------------------------------------------#
# Helper Classes and Functions

class _PrintLogger:
    """ A context manager to sent print statements to the server logs,
        then relinquish control of stdout. """
    
    def __init__(self, log_size=LOG_SIZE, log_count=LOG_COUNT):
        self.log_size = log_size
        self.log_count = log_count

    def write(self, s):
        # Maintain logging system
        if exists(CURRENT_LOG) and getsize(CURRENT_LOG) > self.log_size:
            _cycle_logs(self.log_count)

        # Write to file instead of printing to stdout
        with open(CURRENT_LOG, 'a+') as f:
            f.write(s)

    def __enter__(self): # For 'with' statement
        self.previous_context = sys.stdout
        sys.stdout = self
        return self

    def __exit__(self, ext_type, exc_value, traceback): # For 'with' statement
        # sys.stdout = sys.__stdout__
        sys.stdout = self.previous_context


class _PrintForwarder:
    """ A context manager to send print statements to the client,
        then relinquish control of stdout. """

    def __init__(self, connection=None):
        self.connection = connection

    def write(self, s):
        # Client calls printing to server logs currently disabled here.
        # print(s, file=self.previous_context)
        
        # Print to Client
        _send_msg(self.connection, ['PRINT', s])

    def __enter__(self): # For 'with' statement
        self.previous_context = sys.stdout
        sys.stdout = self
        return self

    def __exit__(self, ext_type, exc_value, traceback): # For 'with' statement
        # sys.stdout = sys.__stdout__
        sys.stdout = self.previous_context

def _wrapped_import(module_name):
    """ Get the module - updated in real time if changed! """
    
    if '.' in module_name or '*' in module_name:
        raise ValueError("Only an individual module from the dragn_modules folder is allowed")
    name = MODULES_FOLDER + "." + module_name
    print("HERE!", name)
    if name in sys.modules and module_name in _module_timestamps:
        # Module had already been loaded
        path = join(MODULES_FOLDER, module_name + ".py")
        # Only reload the module if it has been udpated:
        if max(getctime(path), getmtime(path)) > _module_timestamps[module_name]:
            _module_timestamps[module_name] = time()
            invalidate_caches() # Needed for new .py files to be recognized quickly
            module = reload(sys.modules[name])
        else:
            # No need to reload
            module = sys.modules[name]
    else:
        # Module hadn't been loaded yet
        _module_timestamps[module_name] = time()
        module = import_module(name)

    # Here we enable the module to access certain server functions:
    setattr(module, "SERVER", SERVER) # TODO: put in package __init__?

    return module

def _is_valid_object(obj, module, name, constants=None):
    """ Helper function to weed out other types than functions and constants
        that are supposed to be visible to the server."""
    # NOTE: getmodule throws TypeError for builtins. Covered here.
    return not (ismethod(obj) or isclass(obj) or ismodule(obj) or isbuiltin(obj)) \
        and (name is None or name[0] != '_') \
        and ((not isfunction(obj) and (constants or constants is None)) \
            or (isfunction(obj) and (getmodule(obj) == module)))

def _pull_changes():
    """ Does a Git pull to update modules, without restarting server."""
    subprocess.run(["git", "pull"], subprocess.PIPE)

def _cycle_logs(log_count):
    """ Increments the names of the log files, deleting those >= log_count."""
    names = [f for f in listdir(LOGS_FOLDER) if isfile(join(LOGS_FOLDER, f))]
    nums = []
    for n in names:
        try:
            if n[-4:] == LOG_EXTENSION: nums.append(int(n[:-4]))
        except:
            pass
    nums.sort()
    for n in nums:
        if n >= log_count - 1:
            remove(join(LOGS_FOLDER, str(n) + LOG_EXTENSION))
        else:
            rename(join(LOGS_FOLDER, str(n)   + LOG_EXTENSION),
                      join(LOGS_FOLDER, str(n+1) + LOG_EXTENSION))

def timestamp():
    """ Get a current timestamp."""
    return datetime.now().strftime("%d/%m/%Y %H:%M:%S")


#------------------------------------------------------------------------------#
# Script Behavior:
if __name__ == "__main__":
    if len(sys.argv) > 1:
        server(*sys.argv[1:]) # Specify with an argument in terminal
    else:
        server() # Use default behavior
