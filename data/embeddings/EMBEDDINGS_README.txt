Place embedding .txt files into the txt folder;
If it recognizes the format, the server will be able to load them,
and when they are used it will store an optimized version as a .pkl
in the pkl folder. It will also check modification dates before using them
and update the pkl files accordingly. You need not mess with .pkl files
except to delete old unneeded embeddings which are not being replaced
by new ones with the same name.

In the correct .txt format, each line begins with a word token,
which is followed by spaced out numbers representing the vector.
No multi-spaces.
Header lines (identified by having few entries) will be automatically skipped.