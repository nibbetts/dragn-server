If you're custom loading data and storing it in memory with the models module, you'll likely want to put that data here. Things here are user-managed, nothing is automated.

NOTE that all data and embedding spaces are git-ignored, so if the server is migrated these will need to be done by hand before deleting the old data!
