#! /bin/bash
# This script can be run by hand, or as a cronjob,
#   at startup or every minute or so. See create_cronjob.sh
# Writes the PID of the new server process to .pid

# Ping server and suppress output
python3 dragn_client.py >/dev/null 2>&1

# If no server responded start one, else mention it.
if [ $? -ne 0 ]; then
    nohup python3 dragn_server.py </dev/null >/dev/null 2>&1 &
    echo $! > .pid
    echo "DRAGN Lab Server started"
else
    echo "DRAGN Lab Server already running"
fi
