#! /bin/bash
# Run this script from the dragn-server directory to install the cronjob.
# Careful not to run more than once without personally editing the crontab,
#   beccause I'm not sure but I think you'd have duplicate jobs on the tab.

path=$(pwd)
file="${path}/dragn_startup.sh"
# Copy current cron file info
crontab -l > mycron
# Add to it (and specify not to email the user the output)
echo -e "\n@reboot ${file} >/dev/null 2>&1\n" >> mycron
# Install new cron file
crontab mycron
rm mycron
