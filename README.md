# DRAGN Lab Server / Client #

A server and client, runnable virtually anywhere, but on default settings it only runs in the BYU CS computer network.

## Contents ##

1. Purpose & Use Cases
2. Client Usage
	* Embeddings and Models Modules
3. Server Setup
4. Adding Modules to the Server
	* Modules Accessing Other Modules
	* Using Embedding Spaces, Models, Datasets, and Other Large Objects
	* Fitting Models, Modifying Datasets, and Shared Memory
5. Who do I talk to?

## 1. Purpose & Use Cases

```
"""
    Modules Folder                  Data Folder
    
    ╔════════════╗            ╔════════╗    ╔════════╗        ╔══════════╗
    ║            ║            ║  Data  ║    ║  Data  ║        ║   USER   ║
    ║   Server   ║            ╚═══╤════╝    ╚════╤═══╝        ╚════^═════╝
    ║   Module   ╟----┐           |              |                 |
    ║            ║    |    ╔════════════════════════════╗     ╔════v═════╗
    ╚════╤══^════╝    |    ║      |              |      ║     ║          ║
         |  |         └---->      |    SERVER    |      ║     ║          ║
    ╔════v══╧════╗         ║      |              |      <.....>  CLIENT  ║
    ║            ║         ║      |              |      ║     ║          ║
    ║   Server   ╟--------->  ╔═══v═════╗  ╔═════v═══╗  ║     ║          ║
    ║   Module   ║         ║  ║         ║  ║         ║  ║     ╚════^═════╝
    ║ ╔════════╗ <------------╢  Model  ║  ║  Model  ║  ║          :
    ║ ║ Model  ╟-------------->         ║  ║         ║  ║          :
    ║ ║ Loader ║ ║         ║  ╚═════════╝  ╚═════════╝  ║          :
    ║ ║function║ ║         ║                            ║     ╔════v═════╗
    ║ ╚════════╝ <---------------------------┐          ║     ║  Worker  ║
    ║            ║         ║                 |          ╟----->  Process ║
    ╚════╤══^════╝         ║  ╔═════════╗  ╔═╧═══════╗  ║     ╚══════════╝
         |  |              ║  ║         ║  ║         ║  ║
    ╔════v══╧════╗         ║  ║Embedding║  ║Embedding║  ║
    ║            <------------╢  Space  ║  ║  Space  ║  ║   'USER' could
    ║   Server   ║         ║  ╚═^═══════╝  ╚═══════^═╝  ║   be user-facing
    ║   Module   ╟--------->    |                  |    ║   software.
    ║            ║         ║    |                  |    ║
    ╚════════════╝         ╚════════════════════════════╝   Server manages
                                |                  |        both models &
                                | Embeddings Folder|        embeddings to
                                |                  |        save memory,
    Inter-module imports      ╔═╧═══════╗  ╔═══════╧═╗      and they are
    may also go through       ║Embedding║  ║Embedding║      shared between
    the server.               ║  File   ║  ║  File   ║      modules.
                              ╚═════════╝  ╚═════════╝
"""
```
	
This server-client setup is perhaps best used as a collaborative development tool within a team or a research group. Its purpose is to provide an environment where different researchers'
or developers' code can interact, stay up-to-date, and be easy to access for experiments, even remotely. While it may not be able to serve huge volumes of users at a time, it should
be able to serve multiple users at once well enough even for a lab to use it for some user testing, if it's running on a fairly beefy computer or server.

Depending on the situation or use case, you will want to use its interactive code base differently, choosing carefully when to put your code in the server modules and when to build code around the client:

* For lab members showing off their project, or directly fiddling with it's outputs, or testing module code: Use the client to access server functions.
* For lab members writing and testing new code, especially interacting with or required by other lab members' code: Write server modules that use each other and drop them in the server modules folder; the client is meant for user-facing applications or user-end testing, only. Code you write which uses ouput from the client should not be imported by other people's server module code.
* User studies or product deployment: Put code that needs server functionality in a server module; client-facing code can only access one server function at a time, so if your application needs more than one server function, the client requests to the server should likely should be near the end of the pipeline, not the middle or the beginning. Otherwise, feel free to piece together results from multiple calls if it makes sense to.
* DON'T: write server modules that load a client and make server calls that way.

In short: any code that might be used by other code should go in the server's modules, and any user-facing code can simply make use of client calls.

## 2. Client Usage ##

* A normal user accessing the server remotely only needs the dragn_client.py file.
* If both the client and the server are on the BYU CS network, you need not worry about the address of the server, save the hostname if you're accessing a different computer's server than the default.
* While the server requires Python 3, the client is backwards compatible and may be run from either Python 2 or Python 3.
* Here are a few examples:

``` python
	import dragn_client as dc
	
	# Run a function on the server directly, automating Client creation
	result = dc.run("module_name", "function_name", *args, **kwargs) # For server at default location
	result = dc.run_on("server_name", "module_name", "function_name", *args, **kwargs) # For server with network hostname "server_name"

	# Instantiate a Client
	client = dc.Client() # For server at default location
	client = dc.Client(host_name="server_name") # For server with network hostname "server_name"
	client = dc.Client(address="0.0.0.0") # For a server at IP or web address ("localhost" or "0.0.0.0" for same computer)
	# You can also specify the port, though both server and client may search a little past this number for a valid port,
	#	reducing the need to specify off the default.
	
	# Run a function on the server
	result = client.run("module_name", "function_name", *args, **kwargs) # Run a function on the server
	result = client.run("module_name.function_name", *args, **kwargs) # Also accepts this format
	
	# Test connectivity
	#   Since Client is a class, it survives even if a server is downed and then restarted. (But Client cannot restart server)
	#   A ping is performed automatically at Client instantiation.
	client.ping() # Raises an error if a matching server is not currently receiving on any acceptable port, else does nothing.
	
	# Module, Function, and Constant Documentation
	client.ping() # Test continuing/renewed connectivity to the server. A ping is performed automatically at client creation
	# The following results can also be assigned to variables, but this is less commonly necessary.
	client.list_modules() # Ask the server what modules it has available
	client.list_functions("module_name") # Ask the server what functions are available in the given module
	client.list_constants("module_name") # Ask the server what constants are defined in the given module
	client.get_docs("module_name") # Get description of server module
	client.get_docs("module_name", "function_name") # Get description of function in server module
	client.get_docs("module_name", "constant_name") # Get a string representation of the value of a constant defined in server module
	client.get_docs("module_name.funct_or_const_name") # Also accepts this format

	# Remote Server Management
	#   If server is restarted, Client can be valid again without your having to do anything. However, if the server was
	#	restarted quickly the port number both use may change. Restarting more than once in a few minutes may block all ports temporarily,
	#   preventing the server from starting back up remotely, causing it to essentially crash.
	client.pull_server() # Causes the server to do a Git pull to update modules, not restarting the server itself.
	client.restart_server() # Causes the server to do a Git pull and then restart itself; not usually necessary.
	client.shutdown_server() # Shutdown the server remotely. No Git pull. No remote restart is possible through the client.
	
	# Remote Server Cache Management
	#	Sometimes you need to reset the cached values, because say you broke one by changing it, or for some other reason.
	client.run("embeddings.reset_cache"[, n]) # Reset the embedding space cache, and optionally change its size to n.
	client.run("models.reset_cache"[, n]) # Reset the model and database cache, and optionally change its size to n.
```

* More options can be found in the function and class definitions in the files.
* In order to really make use of the client, you will need to know more about the specific server modules you are accessing. Each module contains its own code documentation, accessible both on BitBucket and through the documentation getter functions in the server, as seen above.
* Embedding spaces and large models and datasets have some special treatment within the server, and have dynamic loading functionalities much like the server modules themselves. For instructions to do with these, see below in this readme, and embeddings/EMBEDDINGS_README.txt, and the documentation in dragn_modules/embeddings.py and dragn_modules/models.py.

#### Embeddings and Models Modules: ####

``` python
	# As these two modules are of particular interest, here are a list of a few functions from each, specifically for use from the Client.
	# See documentation for more functions that are less common, and more details.
	# Here I've used the . format for the run function, just to change things up and clarify what's happening.
	
	client.run("embeddings.list_spaces")								# List available embedding spaces (not just loaded ones).
	client.run("embeddings.in_space",    list_of_words, space='glove')	# Check presence of a few words in a space.
	client.run("embeddings.get_vectors", list_of_words, space='glove')	# Get the vectors for a few words.
	
	client.run("models.list_models")											# List the currently loaded models, datasets, and other large objects.
	client.run("models.model_get",  model_name, attr_name)						# Get a variable's value from a model (for objects, this will only be an ID or signature).
	client.run("models.model_set",  model_name, attr_name, new_value)			# Set the value of a variable in a model (again, only JSON-able types will send properly).
	client.run("models.model_call", model_name, function_name, *args, **kwargs)	# Call a function on a model.
	# Note that there are other reserved argument names (see code documentation for each function).
```

## 3. Server Setup ##

These instructions are for those in charge of running the server itself, or restarting it.

* Clone the repository.
* If you're using a BYU CS networked computer, you don't have to find an IP address, because these computers are assigned hostname.cs.byu.edu, and the server knows that. Otherwise you'll need to know the computer's external IP address, such as "0.0.0.0" in the address parameter.
* To start it in the background, so it won't close when you close the terminal, run the startup script: `$./dragn_startup.sh` from the dragn-server directory. This script first checks for a running server (at the default address), and will only start a new one locally if none is present. This is not yet configured for other addresses. This script also writes the new process PID to `.pid` in case you need to access it without using a client.
* Make sure to install the dependencies of the modules in the dragn_modules folder. Lack of dependencies in the modules will send exceptions to the clients, while lack of dependencies in the server will crash it.
* A simple server/client test module, dragn_modules/server_test, is provided to test for connectivity. Just call one of it's functions from the server. (They can be found by looking at the code or by calling `run("list_functions", "server_test") on a client.)
* To restart or shut down a server, a client can call `client.restart_server()` or `client.shutdown_server()`; see 'Remote Server Management' under 'Client Usage'.
* To set the server to start up automatically on computer reboot, run the script `$create_cronjob.sh` from the dragn-server directory. Do not run more than once. To edit or remove the cronjob it creates, use `$crontab -e`.

To start the server using optional parameters, they are the same as for the server function itself. in bash, this would look something like:
`$./dragn_startup.sh 0.0.0.0 40404 1000000 4` or in python: `server(address=None, port=DEFAULT_PORT, log_size=1000000, log_count=4)`

* `address`: explained above. `localhost` or `0.0.0.0` works for a locally assigned IP address, and is the default.
* `port`: explained above. Defaults to `40404`, and this is the default searched for by the client as well.
* `log_size`: a size threshold beyond which the server will cycle the log files. A given write may take it significantly over this limit if the write is large. Defaults to `1000000`, which is 1 megabyte.
* `log_count`: the number of log files - or the number at which they start getting deleted. To archive a log file you must either change it's extension or rename it to be non purely numeric. Defaults to `4`.

## 4. Adding Modules to the Server ##

To make a new module, simply add a .py file to the dragn_modules folder, and all non-hidden (see below) functions and constants (not including classes and class methods) defined
therein immediately become available to the server, even without restarting it. These can also be changed while the server is live,
which means the server will immediately reflect whatever bugs you introduce into a module (but they shouldn't crash the server, just raise an error).

All print statements that are part of your modules's server-callable functions will be forwarded automatically to the client by the server.
However, print statements made during module initialization and imports will be sent to the server logs.
Currently, behavior for prints made in the loading functions of models (stored in server memory caches) is uncertain; they may be lost entirely or go to the server logs or the client.

WARNING: It is frowned upon to define large variables in modules themselves, external to the functions within the module - this is because they will stay in memory indefinitely!
This feature is used to keep embedding spaces, models, and datasets in memory - but in these cases is carefully controlled with an LRU cache so that a large number of
spaces, models, and datasets does not overwhelm the server, and you are given access to these in order to prevent you
from loading and storing them yourself - making things run much faster and with less memory usage (See: "Using Embedding Spaces, Models, Datasets, and Other Large Objects").
Be conscient that you are sharing the space with others, and what you do is only temporary if it is contained within a function.

All server module code must be Python 3 compatible; while the client is backwards compatible, the server is not, and modules are part of the server, not the client.

Note that your functions must return JSON-able types, or their responses will be converted to something else before being sent.
JSON-able types include, for example, primitives, lists, dictionaries, and strings.
Unfortunately, this means that sets, various other iterables, and even NUMPY NDARRAYS will be CONVERTED TO LISTS, which is slow.
Failing that, an incompatible object will be converted to a string in an attempt to at least send you some form of usable data for an answer.
Attempting to send things such as functions or lambda functions, then, will be like trying to print those objects, and sending the result.

Script-only behavior (such as 'If \_\_name_\_ == "\_\_main\_\_"' blocks) will not be executed by the server upon loading a module. This can be convenient for your own testing.

To hide functions and constants from the server, and thus from client users, name them beginning with an '_'.

#### Modules Accessing Other Modules: ####

This section is not for accessing normal python packages, which are treated normally, but is for other *dragn server* modules specifically
that you want to import and use functions from in your own code.

Each module loaded into the server is given a constant `SERVER` through which it may access certain server functions - namely other server modules and embedding spaces.
While you may use code like`from dragn_modules import other_module`, this does not check if that module has been updated, but will use whatever version the server used last.
Since your code stays in memory, nothing will cause it to use the new version unless something else asks for it on the server,
at which time you will be automatically switched to the updated one. In most cases this is fine, unless you're building two modules in tandem to access each other,
or are using a module rarely called on except by other modules and it needs updated.
If you care, you are instead encouraged to access the other modules' functions through `SERVER`. We also use `SERVER` to access and store large objects we don't want to have to reload;
(see "Using Embedding Spaces, Models, Datasets, and Other Large Objects").

``` python
	# Example code to be found within a custom server module to access other modules' functions and data - THIS IS NOT an example of using the client!
	
	# Uses the module in python's memory without checking for updates
	from dragn_modules import other_module # When testing externally, though, you'll have to run your code from the parent directory or it won't find the module.
	def my_function():
		my_var = other_module.other_function()
	
	# -- OR -- #
	
	# OPTIONAL BLOCK:
	if "SERVER" not in dir(): SERVER = None # Including this is optional, but can remove syntax errors resulting from python not knowing what SERVER is,
	#	since it's put in by the server at runtime. You can further use this block to add alternate behavior,
	#   for example importing other modules directly to allow you to test your code's functionality without the server.
	
	# Modules are updated to most recent versions when calls to SERVER are made to get them
	def my_function(): # But we have to do these 'imports' inside, because having them outside would defeat the purpose of using SERVER, since they wouldn't always be updated.
		other_module = SERVER.get_module("other_module") # Load a module, checking for updates, whereafter you may use it like a normal imported module name.
		my_var = other_module.other_function() # Functions may be called on loaded modules just like normal.
```

For small variables, you don't have to do anything special to make your own module's variables globally accessible, except to define them outside of a function, in the module itself.
Then another module can access them simply by `SERVER.get_module("your_module").variable_name` or by importing your module.
WARNING: we give no guarantee this will work for anything but read-only under the parallel processing conditions of serving multiple clients.
CAUTION: While it is technically possible to use `SERVER.var_name = value` to make variables globally accessible as well, this raises namespace concerns,
and we recommend prepending names to distinguish them - for example, `SERVER.yourmodule_varname = value`.

For large objects that need to be remembered between calls, or take too long to load every single call, see "Using Embedding Spaces, Models, Datasets, and Other Large Objects".

Note that modules are updated in real time by the server, so your calls to `SERVER.get_module` need to occur within function calls, or you may end up running old versions of them.

In general, a good rule of thumb is to have everything be self-contained within the functions you write, having as little as possible defined within the module itself, external to those functions.

#### Using Embedding Spaces, Models, Datasets, and Other Large Objects: ####

In "Adding Modules to the Server" we explained why large variables (generally several megabytes or more) need their own treatment in a server
that manages many pieces of machine learning stuff used by many people. Also, in "Modules Accessing Other Modules" we explained the `SERVER` variable your module will have access to.

``` python
	# Example code to be found within a custom server module to access embedding spaces, other models, and datasets - THIS IS NOT an example of using the client!
	
	from example_package import model_loader, data_loader
	if "SERVER" not in dir(): SERVER = None # OPTIONAL (See "Modules Accessing Other Modules" for explanation)
	
	# Only use large data inside functions. Having these outside would defeat the purpose of using SERVER's cacheing system,
	#	because then your module would keep them in memory anyway:
	def my_function():
		embeddings = SERVER.get_module("embeddings")
		models     = SERVER.get_module("models")
		glove_dict = SERVER.get_space("glove") # Load an embedding space safely through the cacheing system, even parsing if it hasn't been done yet.
		glove_dict = embeddings.get_space("glove") # Same as line above, to show connection in the other server module. SERVER.get_space is a wrapper.
		ml_model   = SERVER.get_model("my_model", model_loader) # Load model safely through the cacheing system. Wrapper for next line.
		ml_model   = models.get_model("my_model", model_loader) # Same as line above, to show connection in the other server module.
		ml_data    = SERVER.get_model("my_data", data_loader) # Datasets with loaders are treated the same as models, and cached the same too.
		
		# Other useful functions. See documentation for more details and functions. Some of these are simple manipulations of glove_dict, nothing more.
		spaces     = embeddings.list_spaces() # List all available embedding spaces (not just loaded ones)
		mask_list  = embeddings.in_space(["honk", "beep"], space='glove') # Check for presence of a few words in a space
		vectors    = embeddings.get_vectors(["honk", "beep"], space='glove') # Get the vectors for a few words
		all_words  = embeddings.get_vocab(space='glove') # Get all words in a space
		output     = ml_model.fit(ml_data) # Retrieved models or datasets can be used normally, including calling their functions.
		output     = models.model_call("my_model", "fit", model_loader) # Same as line above. model_call, model_get, and model_set are mainly for client usage.
		loaded     = models.list_models() # -> [("my_model", "model_loader", ...), ("my_data", "data_loader", ...)]
```
A data generator function could work in place of a loader, but will be run now and then as caches sometimes get invalidated. Therefore, if it takes longer to generate than
to load, we recommend adding your data to the server's `dragn_server/data` folder and creating your own loader function to load it from there.
You can then use that loader function as if it were a model loader.

Edge case: If you somehow have data in memory but it is not loadable from the disk (like some massive global variable),
and it is large enough to be a concern for simply putting in the module and keeping in memory indefinitely that way,
you can use a getter function in place of the loader function. This should only occur if you're accumulating data over the time the server is being used, I think -
at which point you'd need to somehow limit its size yourself.
Note though that if your loader is simply a getter, it will need to be defined in the module, not as a function within a function,
or we cannot guarantee it won't have a dynamically changing address and cause your data to be reloaded every time it is called for anyway
(the cache matches the value name to it's loader function to prevent namespace collisions, and if the loader is changing every time you'll just mess up the cache).

Note that the `embeddings` and `models` modules contain a few additional functions for manipulating and using what they're made for - just like any other server module.
See their .py files for documentation. The `embeddings` module especially has many useful functions, because it loads embedding spaces in a common format
(a simple dictionary of vectors keyed to words, for word embedding spaces) it can do more direct manipulations and getters, whereas the `models` module simply lets you interact
with the current list of objects and loader functions it contains.

If you are setting up models, datasets, or large objects which change over time, this raises some concerns to do with shared memory, and you need to read the next section!

#### Fitting Models, Modifying Datasets, and Shared Memory: ####

Embedding spaces, models, datasets, and other large objects, as well as any global or `SERVER` variables are all in shared memory.
If you are designing a module using and modifying any of these on the fly, then you need to know the implications of this.
We'll use models as an example, as these are often fitted to data after loading them, implying the model is changing in the server's memory.

Anything changeable remains changed. That means, if a model is used by multiple other modules or clients, and one changes it, it will be changed for all of the others as well. Suddenly.
Say you fit a model to some data to run some experiments, and it is of the kind that stays fitted in memory. On your own computer this is not a problem, but on the server,
say someone else decides they need to do the same thing before you finish, using the same model, effectively either resetting and refitting to new data or fitting to additional data
(depending on how that particular model works). Now, when you transform new data according to the model, it will use the other person's fit.
As this is an unacceptable result for everyone involved, the server is not an ideal place for models of this kind. On the other hand, in some internal lab work and experiments
this might be a very useful situation to take advantage of, as a new place to combine people's work and data.
Another common occurrence will be for the server to cycle your module out of the cache because other things need the memory,
causing it to be reloaded from scratch when you call upon it next, minus all your changes, and without even telling you it has done so.
So, modification is essentially only for testing and experimentation purposes.

In most situations on the server, models should not be modified directly by client users, but are rather used by a server module and kept in memory specifically for set purposes.
Once loaded and fit, (typically both done by the loader function you provide to the server,) they are simply used by the functions within your module without changing them.

So, some situations, and how we recommend setting your model usage up for each:

* Fitting data is fast, or no fitting is used: Data can be dynamically fit within functions which make use of the model, and the model loader function can be the standard one it comes with.
* Model is always fit to the same data when used by server modules: Fit the data as part of the model loader function you pass to the server each time you request the model.
* Model has multiple sets of data it is fit to, but those don't change: Again, fit the data as part of the loader functions, only provide a different loader function for each one, and the server will remember multiple different copies of the model.
* Fitting data is slow, but isn't consistently fit to the same data: This is tricky. Build a function which fits the data and performs multiple model calls at once, so you don't waste time refitting data every time, only as often as you need.
* Model is just for experimentation by few individuals: You can make changes to the model without a need to do anything special, but be aware the model might get reloaded out from beneath your feet and lose your changes sometimes.

WARNING: Be aware of how your model works! For example, if a data-fitting function in your model adds to the fitted data instead of re-fits the model entirely,
then you must reset it before each fit of this kind, or you may end up including changes others have made within your own work, that happened in between your own function calls.
In designing your module you must be wise in your use of models, and know what's happening behind the scenes.
If you design it properly, client users theoretically shouldn't have to know this stuff, or worry about how it works behind the scenes and potentially messing it up.

To prevent client users from accessing your model and making changes, preceed the model name with an underscore, and the server will refuse access to model_get, model_set, and model_call.

## 5. Who do I talk to? ##

* Dr. Nancy Fulda, PhD
* Lab personnel: Nathan Tibbetts; nibbetts on GitHub or BitBucket
* https://dragn.ai/